from pyrogram import Client
from urllib.parse import urlparse
import uuid
import subprocess
import os

app = Client("my_bot")

@app.on_message()
def handleMessage(client, message):
    print(message)
    if message.entities is not None:
        print(message.entities)
        for entity in message.entities:
            if entity.type == 'url':
                url = message.text[entity.offset:entity.offset + entity.length]
                processUrl(message, url)


def processUrl(message, url):
    if not url.endswith('webm'):
        print('not webm ' + url)
        message.reply_text('that is not a webm')
    urlComponents = urlparse(url)
    print(urlComponents.hostname)
    if not (urlComponents.hostname.endswith('4chan.org')
        or urlComponents.hostname.endswith('4cdn.org')):
        print('site not supported ' + url)
        message.reply_text('site not supported')
        return
    print('processing ' + url)
    filename = str(uuid.uuid4()) + '.mp4'
    filepath = '/var/www/html/webmfier/' + filename
    print('outputting ' + filename)
    result = subprocess.run(['ffmpeg',
         '-i', url,
         '-vcodec', 'libx264',
         '-preset', 'fast',
         '-profile:v', 'main',
         '-an',
         filepath])
    print(result)
    app.send_animation(chat_id=message.chat.id, animation='/var/www/html/webmfier/' + filename)
    if os.path.exists(filepath):
      os.remove(filepath)

app.run()
